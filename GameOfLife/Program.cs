﻿#region Variables

using System.Text;

bool enabled = true;

int height = 16;
int width = 32;
int speed = 500;
int generation = 1;

var state = GameState.prepare;

#endregion


#region Main logic

Task.Run(KeyManager);

Console.CursorVisible = false;

while (state == GameState.prepare)
{
    PrintPrepare();
    Task.Delay(100).Wait();
    ClearField();
}

ClearField();

bool[,] field = new bool[height, width];

var rnd = new Random();
for (int i = 0; i < height; i++)
{
    for (int j = 0; j < width; j++)
    {
        field[i, j] = rnd.Next(0, 100) > 50 ? true : false;
    }
}

while (state == GameState.game)
{
    PrintStep(field);
    Task.Delay(speed).Wait();
    var nextField = Step(field);
    field = nextField;
    ClearField();
}

Console.WriteLine("GoodBye!");

#endregion


#region Methods

async Task KeyManager()
{
    while (state != GameState.exit)
    {
        if (state == GameState.prepare)
        {
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.Enter:
                    state = GameState.game;
                    break;
                case ConsoleKey.Q:
                    state = GameState.exit;
                    break;
                case ConsoleKey.UpArrow:
                    height--;
                    break;
                case ConsoleKey.DownArrow:
                    height++;
                    break;
                case ConsoleKey.RightArrow:
                    width++;
                    break;
                case ConsoleKey.LeftArrow:
                    width--;
                    break;
                default:
                    break;
            }
        }
        if (state == GameState.game)
        {
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.Q:
                    state = GameState.exit;
                    break;
                case ConsoleKey.UpArrow:
                    speed += 100;
                    break;
                case ConsoleKey.DownArrow:
                    if (speed == 100)
                        break;
                    speed -= 100;
                    break;
                default:
                    break;
            }
        }
    }
}

void ClearField()
{
    Console.Clear();
}

bool[,] Step(bool[,] field)
{
    bool[,] temp = new bool[height, width];

    for (int i = 1; i < height - 1; i++)
    {
        for (int j = 1; j < width - 1; j++)
        {
            bool isAlive = field[i, j];
            int numNeigbours = 0;
            if (field[i - 1, j - 1]) numNeigbours++;
            if (field[i - 1, j]) numNeigbours++;
            if (field[i - 1, j + 1]) numNeigbours++;
            if (field[i, j - 1]) numNeigbours++;
            if (field[i, j + 1]) numNeigbours++;
            if (field[i + 1, j - 1]) numNeigbours++;
            if (field[i + 1, j]) numNeigbours++;
            if (field[i + 1, j + 1]) numNeigbours++;
            bool keepAlive = isAlive && (numNeigbours == 2 || numNeigbours == 3);
            bool makeNewLive = !isAlive && numNeigbours == 3;
            temp[i, j] = keepAlive | makeNewLive;
        }
    }

    generation++;

    return temp;
}

void PrintStep(bool[,] field)
{
    var str = new StringBuilder();

    str.AppendLine(string.Format("Current generation: {0}", generation));
    str.AppendLine(string.Format("Refresh in milliseconds: {0} (up arrow + 100 mls, down arrow - 100 mls)\n", speed));

    for (int i = 0; i < height - 1; i++)
    {
        string line = string.Empty;
        for (int j = 0; j < width - 1; j++)
        {
            line += field[i, j] ? "0" : " ";
        }
        line += "\n";
        str.Append(line);
    }

    str.AppendLine("\n\nPress Q for exit");
    Console.WriteLine(str.ToString());
}

void PrintPrepare()
{
    var str = new StringBuilder();

    str.AppendLine("Hello! Please, set up game field size!");
    str.AppendLine(string.Format("Current height: {0}, width: {1}\n", height, width));

    for (int i = 0; i <= height; i++)
    {
        string line = string.Empty;
        for (int j = 0; j < width; j++)
        {
            if (j == width - 1)
                line += '\n';
            else
            {
                line += '/';
            }
        }
        str.Append(line);
    }

    str.AppendLine("\n\nPress right/left arrow +- width, down/up arrow +- height");
    str.AppendLine("Press Enter for generate random field and start game");
    str.AppendLine("Press Q for exit");

    Console.WriteLine(str.ToString());
}

enum GameState
{
    prepare = 0,
    game = 1,
    exit = 2,
}

#endregion